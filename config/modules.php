<?php

return [
    'article' => [
        'class' => 'app\modules\article\Module',
    ],
    'category' => [
        'class' => 'app\modules\category\Module',
    ],
    'comment' => [
        'class' => 'app\modules\comment\Module',
    ],
];