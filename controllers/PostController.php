<?php

namespace app\controllers;

use app\controllers\base\BaseController;
use app\modules\article\models\Article;
use app\modules\category\models\Category;
use app\modules\comment\models\Comment;
use app\services\PostService;
use Yii;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;

class PostController extends BaseController
{
    public function __construct($id, $module, $config = [], PostService $service)
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        //TODO Высчитывать количество прочитанных новостей при клике на пост и лишнее удалить(Done)
        //TODO Продумать как комментарий сделать, чтобы все оставляли их(Done)


        $data = Article::getAll();
        $categories = Category::getCategoryAll();
        $popular = Article::getPopular();
        $recent = Article::getRecent();

        return $this->render('index', [
            'pagination' => $data['pagination'],
            'items' => $data['items'],
            'recent' => $recent,
            'categories' => $categories,
            'popular' => $popular,
        ]);

    }

    public function actionView($id)
    {
        try {
            $article = $this->service->getArticle($id);
        } catch (NotFoundHttpException $e) {
            throw new NotFoundHttpException();
        }

        if (Yii::$app->request->isPost) {
            $comment = new Comment();
            $bodyParams = Yii::$app->request->post();

            if ($comment->load($bodyParams)){
                $comment->save();
            }
        } else {
            $article->viewed += 1;
            $article->save();

            //TODO Добавить в админку поиск комментариев по постам (Done)
            //TODO Показать комментарий с базы (Done)
            //TODO Отправлять комментарий по кнопке в админку, чтобы была модерация (Done)
        }

        return $this->render('view', $this->service->getViewParams($id, $article));
    }

    public function actionCategory($id)
    {
        $dataCategory = Article::getCategoryPagination($id);
        $recent = Article::getRecent();
        $categories = Category::getCategoryAll();
        $popular = Article::getPopular();

        return $this->render('category', [
            'items' => $dataCategory['items'],
            'pagination' => $dataCategory['pagination'],
            'recent' => $recent,
            'categories' => $categories,
            'popular' => $popular
        ]);
    }


//    /**
//     * @param string $category
//     * @inheritdoc
//     */
//    public function behaviors()
//    {
//        return [
//            'access' => [
//                'class' => AccessControl::class,
//                'only' => ['logout'],
//                'rules' => [
//                    [
//                        'actions' => ['logout'],
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],
//            'verbs' => [
//                'class' => VerbFilter::class,
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
//        ];
//    }

//    /**
//     * @inheritdoc
//     */
//    public function actions()
//    {
//        return [
//            'error' => [
//                'class' => 'yii\web\ErrorAction',
//            ],
//            'captcha' => [
//                'class' => 'yii\captcha\CaptchaAction',
//                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
//            ],
//        ];
//    }

    /**
     * Login action.
     *
     * @return Response|string
     */
}
