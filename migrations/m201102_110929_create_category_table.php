<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%category}}`.
 */
class m201102_110929_create_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%category}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->comment('Название категории'),
            'is_active' => $this->smallInteger(1)->defaultValue(1)->comment('Активность'),
            'position' => $this->integer()->comment('Очередность'),
            'created_at' => $this->dateTime()->comment('Время создания'),
            'updated_at' => $this->dateTime()->comment('Время редактирования'),
        ]);
        $this->createIndex('idx-is_active', 'category', 'is_active');
    }

        public function safeDown()
    {

        $this->dropIndex('idx-is_active','category');
        $this->dropTable('{{%category}}');

    }
}
