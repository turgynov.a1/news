<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%article}}`.
 */
class m201102_115254_create_article_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%article}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->comment('Категории'),
            'created_at' => $this->dateTime()->comment('Время создания'),
            'updated_at' => $this->dateTime()->comment('Время редактирования'),
            'is_active' => $this->smallInteger(1)->defaultValue(1)->comment('Активность'),
            'position' => $this->integer()->comment('Очередность'),

        ]);

        $this->createIndex('idx-is_active', 'article', 'is_active');

        $this->createTable('article_translation', [
            'id' => $this->primaryKey(),
            'lang' => $this->string()->comment('Язык перевода'),
            'article_id' => $this->integer()->comment('Новость'),
            'title' => $this->string(255) ->comment('Название'),
            'text' => $this->text()->notNull()->comment('Полный текст'),
            'image' => $this->string(255) -> comment('Изображение'),
        ]);

        $this->createIndex(
            'idx-article_translation-article-id',
            'article_translation',
            'article_id'
        );

        $this->addForeignKey(
            'fk-article_translation-article_id',
            'article_translation',
            'article_id',
            'article',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-article_category_id',
            'article',
            'category_id',
            'category',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->dropForeignKey(
            'fk-article_translation-article_id',
            'article_translation'
        );

        $this->dropForeignKey(
            'fk-article_category_id',
            'article'
        );


        $this->dropIndex(
            'idx-article_translation-article-id',
            'article_translation'

        );

        $this->dropIndex(
            'idx-is_active',
            'article'

        );

        $this->dropTable('article_translation');


        $this->dropTable('{{%article}}');
    }

}
