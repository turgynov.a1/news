<?php

use yii\db\Migration;

/**
 * Class m201103_054719_alter_category_table
 */
class m201103_054719_alter_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable('category_translation', [
            'id' => $this->primaryKey(),
            'lang' => $this->string()->comment('Язык перевода'),
            'category_id' => $this->integer()->comment('Категория'),
            'title' => $this->string(255) ->comment('Название'),
            'text' => $this->text()->notNull()->comment('Полный текст'),
            'image' => $this->string(255) -> comment('Изображение'),
        ]);

        $this->createIndex(
            'idx-category_translation-category-id',
            'category_translation',
            'category_id'
        );

        $this->addForeignKey(
            'fk-category_translation-category_id',
            'category_translation',
            'category_id',
            'category',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-category_translation-category_id',
            'category_translation'
        );


        $this->dropIndex(
            'idx-category_translation-category-id',
            'category_translation'

        );

        $this->dropIndex(
            'idx-is_active',
            'category'

        );

        $this->dropTable('category_translation');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201103_054719_alter_category_table cannot be reverted.\n";

        return false;
    }
    */
}
