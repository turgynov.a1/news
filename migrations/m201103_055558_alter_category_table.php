<?php

use yii\db\Migration;

/**
 * Class m201103_055558_alter_category_table
 */
class m201103_055558_alter_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('category', 'title');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('category','title');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201103_055558_alter_category_table cannot be reverted.\n";

        return false;
    }
    */
}
