<?php

use yii\db\Migration;

/**
 * Class m201107_094451_alter_article_table
 */
class m201107_094451_alter_article_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
            $this->addColumn('article','viewed', $this->integer()->comment('Просмотрены'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
            $this->dropColumn('article', 'viewed');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201107_094451_alter_article_table cannot be reverted.\n";

        return false;
    }
    */
}
