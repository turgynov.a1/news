<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%comment}}`.
 */
class m201109_094445_create_comment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%comment}}', [
            'id' => $this->primaryKey(),
            'article_id' => $this->integer()->comment('Пост'),
            'created_at' => $this->dateTime()->comment('Время создания'),
            'is_active' => $this->smallInteger(1)->defaultValue(0)->comment('Активность'),
            'name' => $this->string()->comment('Имя'),
            'text' => $this->text()->notNull()->comment('Полный текст')
        ]);
        $this->createIndex('idx-is_active', 'comment', 'is_active');

    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx-is_active','comment');
        $this->dropTable('{{%comment}}');
    }
}
