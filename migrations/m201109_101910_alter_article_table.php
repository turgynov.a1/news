<?php

use yii\db\Migration;

/**
 * Class m201109_101910_alter_article_table
 */
class m201109_101910_alter_article_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('article', 'comment_id',$this->integer()->comment('Комментарий'));
        $this->addForeignKey(
            'fk-article_comment_id',
            'article',
            'comment_id',
            'comment',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('article', 'comment_id');
       $this->dropForeignKey('fk-article_comment_id', 'article');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201109_101910_alter_article_table cannot be reverted.\n";

        return false;
    }
    */
}
