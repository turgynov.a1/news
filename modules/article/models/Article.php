<?php

namespace app\modules\article\models;

use app\modules\category\models\Category;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\data\Pagination;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "article".
 *
 * @property int $id
 * @property int $category_id Категории
 * @property string $created_at Время создания
 * @property string $updated_at Время редактирования
 * @property int $is_active Активность
 * @property int $position Очередность
 * @property ArticleTranslation[] $articleTranslations
 *  @property ArticleTranslation $articleTranslation
 */
class Article extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'article';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'is_active', 'position', 'viewed'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['category_id'], 'required'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('myadmin', 'ID'),
            'category_id' => Yii::t('myadmin', 'Категории'),
            'created_at' => Yii::t('myadmin', 'Время создания'),
            'updated_at' => Yii::t('myadmin', 'Время редактирования'),
            'is_active' => Yii::t('myadmin', 'Активность'),
            'position' => Yii::t('myadmin', 'Очередность'),
            'viewed' => Yii::t('myadmin', 'Просмотрены'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleTranslations()
    {
        return $this->hasMany(ArticleTranslation::className(), ['article_id' => 'id']);
    }

    public function getArticleTranslation()
    {
        return $this->hasOne(ArticleTranslation::class, ['article_id' => 'id'])->where(['lang' => Yii::$app->language]);
    }

    /**
     * {@inheritdoc}
     * @return ArticleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ArticleQuery(get_called_class());
    }

    public function getDate()
    {
        return Yii::$app->formatter->asDate($this->created_at);
    }

    public static function getPopular()
    {
        return Article::find()->orderBy('viewed desc')->limit(3)->all();
    }

    public static function getRecent()
    {
        return Article::find()->orderBy('created_at')->limit(2)->all();
    }

    public static function getAll($pageSize = 5)
    {
        $query = Article::find()->where(['is_active' => 1]);
        $count = $query->count();
        $pages = new Pagination(['totalCount' => $count, 'pageSize'=>$pageSize]);
        $items = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy('position ASC')
            ->all();
        $data['items'] = $items;
        $data['pagination'] = $pages;

        return $data;
    }

    public static function getCategoryPagination($id,$pageSize = 4)
    {
        $query = Article::find()->where(['category_id' => $id]);
        $count = $query->count();
        $pages = new Pagination(['totalCount' => $count, 'pageSize'=>$pageSize]);
        $items = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy('position ASC')
            ->all();

        $dataCategory['items'] = $items;
        $dataCategory['pagination'] = $pages;

        return $dataCategory;
    }

    public function behaviors()
    {
        return [
            'translation' => [
                'class' => 'Bridge\Core\Behaviors\TranslationBehavior',
                'translationModelClass' => ArticleTranslation::class,
                'translationModelRelationColumn' => 'article_id'
            ],
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                'value' => new Expression('CURRENT_TIMESTAMP()'),
            ],
            'positionSort' => [
                'class' => 'yii2tech\ar\position\PositionBehavior',
                'positionAttribute' => 'position',
            ],
            'metaTag' => [
                'class' => 'Bridge\Core\Behaviors\MetaTagBehavior',
                'titleColumn' => 'translation.title',
                'descriptionColumn' => 'translation.description',
            ],
        ];
    }
}
