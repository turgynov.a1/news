<?php

namespace app\modules\article\models;

use Yii;

/**
 * This is the model class for table "article_translation".
 *
 * @property int $id
 * @property string $lang Язык перевода
 * @property int $article_id Новость
 * @property string $title Название
 * @property string $text Полный текст
 * @property string $image Изображение
 *
 * @property Article $article
 */
class ArticleTranslation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'article_translation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['article_id'], 'integer'],
            [['text'], 'required'],
            [['text'], 'string'],
            [['image'], 'file', 'on' => ['create', 'update'], 'extensions' => ['gif', 'jpg', 'png', 'jpeg']],
            [['lang', 'title'], 'string', 'max' => 255],
            [['article_id'], 'exist', 'skipOnError' => true, 'targetClass' => Article::className(), 'targetAttribute' => ['article_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('myadmin', 'ID'),
            'lang' => Yii::t('myadmin', 'Язык перевода'),
            'article_id' => Yii::t('myadmin', 'Новость'),
            'title' => Yii::t('myadmin', 'Название'),
            'text' => Yii::t('myadmin', 'Полный текст'),
            'image' => Yii::t('myadmin', 'Изображение'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['id' => 'article_id']);
    }

    /**
     * {@inheritdoc}
     * @return ArticleTranslationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ArticleTranslationQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            'imageUpload' => [
                'class' => 'Bridge\Core\Behaviors\BridgeUploadImageBehavior',
                'isTranslation' => true,
                'instanceByName' => true,
                'attribute' => 'image',
                'path' => '@webroot/media/article_translation/{id}',
                'url' => '@web/media/article_translation/{id}',
                'scenarios' => ['create', 'update', 'default'],

            ],
        ];
    }

    public function getImagePath()
    {
        if(empty($this->image)){
            return '';
        }
        return '/media/article_translation/' . $this->id . '/' . $this->image;
    }

}
