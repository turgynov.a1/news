<?php

namespace app\modules\article\models;

/**
 * This is the ActiveQuery class for [[ArticleTranslation]].
 *
 * @see ArticleTranslation
 */
class ArticleTranslationQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return ArticleTranslation[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return ArticleTranslation|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
