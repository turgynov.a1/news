<?php
$translationModel = $model->getTranslation($languageCode);
?>


<div class="row">
    <div class="col-md-8">

        <?= $form->field($translationModel, '[' . $languageCode . ']article_id')->hiddenInput()->label(false) ?>

        <?= $form->field($translationModel, '['. $languageCode .']title')->textInput() ?>


        <?= $form->field($translationModel, '['. $languageCode .']text')->richTextArea() ?>

    </div>

    <div class="col-md-4">

        <?= $form->field($translationModel, '[' . $languageCode . ']image')->imageUpload([
            'pluginOptions' => [
                'deleteUrl' => \yii\helpers\Url::to([
                    '/admin/base-admin/delete-file',
                    'id' => $translationModel->id,
                    'modelName' => $translationModel::className(),
                    'behaviorName' => 'imageUpload',
                ]),
            ]
        ]) ?>

    </div>
</div>


