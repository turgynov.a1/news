<?php

use app\modules\category\models\Category;
use yii\bootstrap\Html;
use Bridge\Core\Widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\article\models\Article */
/* @var $form Bridge\Core\Widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-md-8">

        <?= $form->translate($model, '@app/modules/article/views/default/_form-translate') ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'is_active')->switchInput() ?>


        <?= $form->field($model, 'category_id')->select2(
            ArrayHelper::map(Category::find()->where(['is_active'=>true])->all(),
                'id', 'translation.title'),
            [
                'options' => [
                    'style'=>['; display:inline !important'],
                    'placeholder' => Yii::t('myadmin', 'Выберите...'),
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
        ?>
    </div>
</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('myadmin', 'Create') : Yii::t('myadmin', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
<?php ActiveForm::end(); ?>
