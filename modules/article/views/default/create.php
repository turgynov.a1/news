<?php

/* @var $this yii\web\View */
/* @var $model app\modules\article\models\Article */

$this->title = Yii::t('myadmin', 'Create Article');
$this->params['breadcrumbs'][] = ['label' => Yii::t('myadmin', 'Articles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

