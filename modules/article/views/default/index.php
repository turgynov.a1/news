<?php

use app\modules\category\models\Category;
use dosamigos\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii2tech\admin\grid\ActionColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\article\models\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('myadmin', 'Articles');
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['create'],
];
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['class' => 'grid-view table-responsive'],
    'behaviors' => [
        \dosamigos\grid\behaviors\ResizableColumnsBehavior::class
    ],
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        [
            'attribute' => 'title',
            'label' => Yii::t('app', 'Название'),
            'value' => function($model){
                return $model->articleTranslation->title;
            },
            'format' => 'ntext',
            'contentOptions'=>['style'=>'white-space: normal;'],
        ],
        [
            'attribute' => 'category_id',
            'value' => function ($data) {
                return ($data->category_id !== null) ? $data->category->categoryTranslation->title : '-';
            },
            'filter' => Html::activeDropDownList($searchModel, 'category_id',
                ArrayHelper::map(Category::find()
                    ->where(['is_active'=>true])
                    ->all(),
                    'id', 'translation.title'),
                [
                    'prompt' => 'Выберите ...',
                    'class' => 'form-control'
                ])
        ],
        'created_at',
        'viewed',
        [
            'class' => 'dosamigos\grid\columns\ToggleColumn',
            'attribute' => 'is_active',
            'onValue' => 1,
            'onLabel' => 'Active',
            'offLabel' => 'Not active',
            'contentOptions' => ['class' => 'text-center'],
            'afterToggle' => 'function(r, data){if(r){console.log("done", data)};}',
            'filter' => ['1' => 'Active', '0' => 'Not active'],
        ],
         [
             'class' => 'yii2tech\admin\grid\PositionColumn',
             'value' => 'position',
             'template' => '<div class="btn-group">{first}&nbsp;{prev}&nbsp;{next}&nbsp;{last}</div>',
             'buttonOptions' => ['class' => 'btn btn-info btn-xs'],
         ],

        [
            'class' => ActionColumn::class,
        ],
    ],
]); ?>
