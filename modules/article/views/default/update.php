<?php

/* @var $this yii\web\View */
/* @var $model app\modules\article\models\Article */

$this->title = Yii::t('myadmin', 'Update Article: ') . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('myadmin', 'Articles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('myadmin', 'Update');
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>


