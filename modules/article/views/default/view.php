<?php

use yii\bootstrap\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\article\models\Article */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('myadmin', 'Articles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['update', 'id' => $model->id],
    ['delete', 'id' => $model->id]
];
?>
<div class="row">
    <div class="col-lg-8 detail-view-wrap">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'category_id',
            'created_at',
            'updated_at',
            'is_active',
            'position',
            [
                'class' => 'Bridge\Core\Widgets\Columns\TruncatedTextColumn',
                'label' => Yii::t('myadmin', 'Категория'),
                'value' =>  function ($data) {
                    return $data->category_id ? $data->category->title : '-';
                },
            ],
        ],
    ]) ?>
    </div>
</div>