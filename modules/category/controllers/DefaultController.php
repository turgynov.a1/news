<?php

namespace app\modules\category\controllers;

use Bridge\Core\Controllers\BaseAdminController;
use yii\helpers\ArrayHelper;
use yii2tech\admin\actions\Position;
use dosamigos\grid\actions\ToggleAction;

/**
 * DefaultController implements the CRUD actions for [[app\modules\category\models\Category]] model.
 * @see app\modules\category\models\Category
 */
class DefaultController extends BaseAdminController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'app\modules\category\models\Category';
    /**
     * @inheritdoc
     */
    public $searchModelClass = 'app\modules\category\models\CategorySearch';




    /**
     * @inheritdoc
     */
    public function actions()
    {
        return ArrayHelper::merge(
            parent::actions(),
            [
                'toggle' => [
                    'class' => ToggleAction::class,
                    'modelClass' => 'app\modules\category\models\Category',
                    'onValue' => 1,
                    'offValue' => 0
                ],
                'position' => [
                    'class' => Position::class,
                ],
            ]
        );
    }
}
