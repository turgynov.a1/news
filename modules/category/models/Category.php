<?php

namespace app\modules\category\models;

use app\modules\article\models\Article;
use app\modules\article\models\ArticleTranslation;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property int $is_active Активность
 * @property int $position Очередность
 * @property string $created_at Время создания
 * @property string $updated_at Время редактирования
 * @property Article[] $articles
 * @property CategoryTranslation[] $categoryTranslations
 * @property CategoryTranslation $categoryTranslation
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['is_active', 'position'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('myadmin', 'ID'),
            'is_active' => Yii::t('myadmin', 'Активность'),
            'position' => Yii::t('myadmin', 'Очередность'),
            'created_at' => Yii::t('myadmin', 'Время создания'),
            'updated_at' => Yii::t('myadmin', 'Время редактирования'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Article::className(), ['category_id' => 'id']);
    }

    public static function getCategoryAll()
    {
      return  Category::find()->orderBy('position ASC')->all();
    }


    /*public function getArticleTranslations()
    {
        return $this->hasMany(ArticleTranslation::className(), ['article_id' => 'id']);
    }

    public function getArticleTranslation()
    {
        return $this->hasOne(ArticleTranslation::class, ['article_id' => 'id'])->where(['lang' => Yii::$app->language]);
    }*/

    public function getCategoryTranslations()
    {
        return $this->hasMany(CategoryTranslation::className(), ['category_id' => 'id']);
    }

    public function getCategoryTranslation()
    {
        return $this->hasOne(CategoryTranslation::class, ['category_id' => 'id'])->where(['lang' => Yii::$app->language]);
    }


   /* public static function NewsCategories()
    {
        return Catalog::find()->where(['module'=>4])->andWhere(['is_active'=>true])->all();
    }*/

    public static function getCategories()
    {
        return Category::find()->where(['is_active'=>true])->all();
    }
    /**
     * {@inheritdoc}
     * @return CategoryQuery the active query used by this AR class.
     */



    public static function find()
    {
        return new CategoryQuery(get_called_class());
    }
    public function behaviors()
    {
        return [
            'translation' => [
                'class' => 'Bridge\Core\Behaviors\TranslationBehavior',
                'translationModelClass' => CategoryTranslation::class,
                'translationModelRelationColumn' => 'category_id'
            ],
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                'value' => new Expression('CURRENT_TIMESTAMP()'),
            ],
            'positionSort' => [
                'class' => 'yii2tech\ar\position\PositionBehavior',
                'positionAttribute' => 'position',
            ],
            'metaTag' => [
                'class' => 'Bridge\Core\Behaviors\MetaTagBehavior',
                'titleColumn' => 'translation.title',
                'descriptionColumn' => 'translation.description',
            ],
        ];
    }
}
