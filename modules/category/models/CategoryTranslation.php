<?php

namespace app\modules\category\models;

use Yii;

/**
 * This is the model class for table "category_translation".
 *
 * @property int $id
 * @property string $lang Язык перевода
 * @property int $category_id Категория
 * @property string $title Название
 * @property string $text Полный текст
 * @property string $image Изображение
 *
 * @property Category $category
 */
class CategoryTranslation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category_translation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id'], 'integer'],
            [['text'], 'required'],
            [['text'], 'string'],
            [['lang', 'title',], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('myadmin', 'ID'),
            'lang' => Yii::t('myadmin', 'Язык перевода'),
            'category_id' => Yii::t('myadmin', 'Категория'),
            'title' => Yii::t('myadmin', 'Название'),
            'text' => Yii::t('myadmin', 'Полный текст'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * {@inheritdoc}
     * @return CategoryTranslationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CategoryTranslationQuery(get_called_class());
    }
}
