<?php
$translationModel = $model->getTranslation($languageCode);
?>


<div class="row">
    <div class="col-md-8">

        <?= $form->field($translationModel, '[' . $languageCode . ']category_id')->hiddenInput()->label(false) ?>

        <?= $form->field($translationModel, '['. $languageCode .']title')->textInput() ?>


        <?= $form->field($translationModel, '['. $languageCode .']text')->richTextArea() ?>

    </div>

    <div class="col-md-4">

    </div>
</div>


