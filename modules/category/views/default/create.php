<?php

/* @var $this yii\web\View */
/* @var $model app\modules\category\models\Category */

$this->title = Yii::t('myadmin', 'Create Category');
$this->params['breadcrumbs'][] = ['label' => Yii::t('myadmin', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?= $this->render('_form', [
    'model' => $model,
]) ?>

