<?php

/* @var $this yii\web\View */
/* @var $model app\modules\category\models\Category */

$this->title = Yii::t('myadmin', 'Update Category: ') . $model->categoryTranslation->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('myadmin', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->categoryTranslation->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('myadmin', 'Update');
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>


