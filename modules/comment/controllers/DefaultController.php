<?php

namespace app\modules\comment\controllers;

use Bridge\Core\Controllers\BaseAdminController;
use yii\helpers\ArrayHelper;
use yii2tech\admin\actions\Position;
use dosamigos\grid\actions\ToggleAction;

/**
 * DefaultController implements the CRUD actions for [[app\modules\comment\models\Comment]] model.
 * @see app\modules\comment\models\Comment
 */
class DefaultController extends BaseAdminController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'app\modules\comment\models\Comment';
    /**
     * @inheritdoc
     */
    public $searchModelClass = 'app\modules\comment\models\CommentSearch';




    /**
     * @inheritdoc
     */
    public function actions()
    {
        return ArrayHelper::merge(
            parent::actions(),
            [
                'toggle' => [
                    'class' => ToggleAction::class,
                    'modelClass' => 'app\modules\comment\models\Comment',
                    'onValue' => 1,
                    'offValue' => 0
                ],
                'position' => [
                    'class' => Position::class,
                ],
            ]
        );
    }
}
