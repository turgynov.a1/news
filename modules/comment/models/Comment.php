<?php

namespace app\modules\comment\models;

use app\modules\article\models\Article;
use app\modules\article\models\ArticleTranslation;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "comment".
 *
 * @property int $id
 * @property int $article_id Пост
 * @property string $created_at Время создания
 * @property int $is_active Активность
 * @property string $name Имя
 * @property string $text Полный текст
 *
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['article_id', 'is_active'], 'integer'],
            [['created_at'], 'safe'],
            [['name','text', 'article_id'], 'required'],
            [['text'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('myadmin', 'ID'),
            'article_id' => Yii::t('myadmin', 'Пост'),
            'created_at' => Yii::t('myadmin', 'Время создания'),
            'is_active' => Yii::t('myadmin', 'Активность'),
            'name' => Yii::t('myadmin', 'Имя'),
            'text' => Yii::t('myadmin', 'Полный текст'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Article::class, ['comment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Article::class, ['id' => 'article_id']);
    }

    public function getDate()
    {
        return Yii::$app->formatter->asDate($this->created_at);
    }

    public static function getArticleCommentCount($id)
    {
       return Comment::find()->where(['is_active' => 1, 'article_id' => $id])->count();
    }

    public static function getComments($id)
    {
        return Comment::find()->where(['is_active' => 1, 'article_id' => $id])->all();
    }

    public function behaviors()
    {
        return [

            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                'value' => new Expression('CURRENT_TIMESTAMP()'),
            ],

        ];
    }

    /**
     * {@inheritdoc}
     * @return CommentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CommentQuery(get_called_class());
    }

    public function getArticleTitle()
    {
        return $this->article->articleTranslation->title;
    }
}
