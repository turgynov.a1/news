<?php

use app\modules\article\models\Article;
use app\modules\comment\models\Comment;
use yii\bootstrap\Html;
use Bridge\Core\Widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\comment\models\Comment */
/* @var $form Bridge\Core\Widgets\ActiveForm */

?>

<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-md-8">

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'text')->richTextArea(['options' => ['rows' => 6]]) ?>


    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'is_active')->switchInput() ?>
        <?= $form->field($model, 'article_id')->select2(
            ArrayHelper::map(Article::find()->where(['is_active'=>true])->all(),
                'id', 'translation.title'),
            [
                'options' => [
                    'style'=>['; display:inline !important'],
                    'placeholder' => Yii::t('myadmin', 'Выберите...'),
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
        ?>
    </div>
</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('myadmin', 'Create') : Yii::t('myadmin', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
<?php ActiveForm::end(); ?>
