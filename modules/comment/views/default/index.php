<?php

use app\modules\article\models\Article;
use app\modules\comment\models\Comment;
use dosamigos\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii2tech\admin\grid\ActionColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\comment\models\CommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('myadmin', 'Comments');
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['create'],
];
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['class' => 'grid-view table-responsive'],
    'behaviors' => [
        \dosamigos\grid\behaviors\ResizableColumnsBehavior::class
    ],
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',

        [
            'attribute' => 'article_id',
            'value' => 'articleTitle',
            'filter' => Html::activeDropDownList($searchModel, 'article_id',
                ArrayHelper::map(Article::find()
                    ->where(['is_active'=>true])
                    ->all(),
                    'id', 'translation.title'),
                [
                    'prompt' => 'Выберите ...',
                    'class' => 'form-control'
                ])
        ],
        'created_at',
        [
            'class' => 'dosamigos\grid\columns\ToggleColumn',
            'attribute' => 'is_active',
            'onValue' => 1,
            'onLabel' => 'Active',
            'offLabel' => 'Not active',
            'contentOptions' => ['class' => 'text-center'],
            'afterToggle' => 'function(r, data){if(r){console.log("done", data)};}',
            'filter' => ['1' => 'Active', '0' => 'Not active'],
        ],
        'name',
         [
             'class' => 'Bridge\Core\Widgets\Columns\TruncatedTextColumn',
             'attribute' => 'text',
         ],

        [
            'class' => ActionColumn::class,
        ],
    ],
]); ?>
