<?php

/* @var $this yii\web\View */
/* @var $model app\modules\comment\models\Comment */

$this->title = Yii::t('myadmin', 'Update Comment: ') . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('myadmin', 'Comments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('myadmin', 'Update');
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>


