<?php

use yii\bootstrap\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\comment\models\Comment */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('myadmin', 'Comments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['update', 'id' => $model->id],
    ['delete', 'id' => $model->id]
];
?>
<div class="row">
    <div class="col-lg-8 detail-view-wrap">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'article_id',
            'created_at',
            'is_active',
            'name',
            'text:ntext',
        ],
    ]) ?>
    </div>
</div>