<?php

namespace app\services;

use app\modules\article\models\Article;
use app\modules\category\models\Category;
use app\modules\comment\models\Comment;
use yii\web\NotFoundHttpException;

class PostService
{
    /**
     * @param $id - {@see Article::$id}
     * @param $article - {@see Article}
     * @return array
     */
    public function getViewParams($id, $article)
    {
        $comments = Comment::getComments($id);
        $articleCommentCount = Comment::getArticleCommentCount($id);
        $recent = Article::getRecent();
        $categories = Category::getCategoryAll();
        $popular = Article::getPopular();
        $comment = new Comment();

        return [
            'article' => $article,
            'recent' => $recent,
            'categories' => $categories,
            'comments' => $comments,
            'articleCommentCount' => $articleCommentCount,
            'popular' => $popular,
            'comment' => $comment
        ];
    }

    /**
     * @param $id - {@see Article::$id}
     * @return Article
     */
    public function getArticle($id)
    {
        $article = Article::findOne($id);

        if (empty($article)) {
            throw new NotFoundHttpException();
        }

        return $article;
    }

//    public function insertComment()
//    {
//
//    }
//
//    public function incrementViews()
//    {
//
//    }
}