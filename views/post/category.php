<?php use yii\helpers\Url;
use yii\widgets\LinkPager;

?>

<!--main content start-->
<div class="main-content">
    <div class="container">
        <div class="row">
            <div class="col-md-8">

                <?php foreach ($items as $item) :?>
                <article class="post post-list">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="post-thumb">
                                <a href="<?= Url::toRoute(['post/view', 'id' =>$item->id])  ?>"><img src="<?= $item->translation->getImagePath(); ?>" alt="" class="pull-left"></a>

                                <a href="<?= Url::toRoute(['post/view', 'id' =>$item->id])  ?>" class="post-thumb-overlay text-center">
                                    <div class="text-uppercase text-center">View Post</div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="post-content">
                                <header class="entry-header text-uppercase">
                                    <h6><a href="<?= Url::toRoute(['post/category', 'id' =>$item->category->id])  ?>"> <?= $item->category->translation->title; ?></a></h6>

                                    <h1 class="entry-title"><a href="<?= Url::toRoute(['post/view', 'id' =>$item->id])  ?>"><?= $item->translation->title ?> </a></h1>
                                </header>
                                <div class="entry-content">
                                    <p>
                                        <?= $item->translation->text ?>
                                    </p>
                                </div>
                                <div class="social-share">
                                    <span class="social-share-title pull-left text-capitalize">By Rubel On <?= $item->getDate(); ?></span>

                                </div>
                            </div>
                        </div>
                    </div>
                </article>
                <?php endforeach;?>
                <?php
                echo LinkPager::widget([
                    'pagination' => $pagination,
                ]);
                ?>
            </div>
            <?= $this->render('/layouts/sidebar' , [
                'recent' => $recent,
                'categories' => $categories,
                'popular' => $popular,
            ]); ?>
        </div>
    </div>
</div>
<!-- end main content-->
<!--footer start-->