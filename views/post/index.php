<?php

use yii\helpers\Url;
use yii\widgets\LinkPager;

?>

<!--main content start-->
<div class="main-content">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <?php foreach ($items as $post) : ?>
                    <article class="post">
                        <div class="post-thumb">
                            <a href="<?= Url::toRoute(['post/view', 'id'=>$post->id]); ?>"><img src="<?= $post->translation->getImagePath(); ?>" alt=""></a>

                            <a href="<?= Url::toRoute(['post/view', 'id'=>$post->id]); ?>" class="post-thumb-overlay text-center">
                                <div class="text-uppercase text-center">View Post</div>
                            </a>
                        </div>
                        <div class="post-content">
                            <header class="entry-header text-center text-uppercase">
                                <h6><a href="<?= Url::toRoute(['post/category', 'id'=>$post->category->id]) ?>"> <?= $post->category->translation->title; ?></a></h6>

                                <h1 class="entry-title"><a href="<?= Url::toRoute(['post/view', 'id'=>$post->id]); ?>"><?= $post->translation->title ?></a></h1>


                            </header>
                            <div class="entry-content">
                                <p>
                                    <?= $post->translation->text ?>
                                </p>

                                <div class="btn-continue-reading text-center text-uppercase">
                                    <a href="<?= Url::toRoute(['post/view', 'id'=>$post->id]); ?>" class="more-link">Continue Reading</a>
                                </div>
                            </div>
                            <div class="social-share">
                                <span class="social-share-title pull-left text-capitalize">By <a href="#">Rubel</a> On <?= $post->getDate(); ?></span>
                                <ul class="text-center pull-right">
                                    <li><a class="s-facebook" href="#"><i class="fa fa-eye"></i></a></li><?= $post->viewed ?>
                                </ul>
                            </div>
                        </div>
                    </article>
                <?php endforeach; ?>
                <?php
                echo LinkPager::widget([
                    'pagination' => $pagination,
                ]);
                ?>
            </div>
            <?= $this->render('/layouts/sidebar' , [
                'recent' => $recent,
                'categories' => $categories,
                'popular' => $popular,
            ]); ?>
        </div>
    </div>
</div>
<!-- end main content-->
<!--footer start-->>
