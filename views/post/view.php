<?php

use app\modules\comment\models\Comment;
use Bridge\Core\Widgets\ActiveForm;
use yii\helpers\Url;

/** @var Comment $comment */
/**  */

?>

<!--main content start-->
<div class="main-content">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <article class="post">
                    <div class="post-thumb">
                        <a href="blog.html"><img src="<?= $article->translation->getImagePath(); ?>" alt=""></a>
                    </div>
                    <div class="post-content">
                        <header class="entry-header text-center text-uppercase">
                            <h6>
                                <a href="<?= Url::toRoute(['post/category', 'id' => $article->category->id]) ?>"> <?= $article->category->translation->title ?></a>
                            </h6>

                            <h1 class="entry-title"><a
                                        href="<?= Url::toRoute(['post/view', 'id' => $article->id]) ?>"><?= $article->translation->title ?></a>
                            </h1>


                        </header>
                        <div class="entry-content">
                            <?= $article->translation->text ?>
                        </div>

                        <div class="social-share">
							<span
                                    class="social-share-title pull-left text-capitalize">By Rubel On <?= $article->getDate(); ?></span>

                        </div>
                    </div>
                </article>
                <h4><?= $articleCommentCount ?> Comments</h4>
                <?php foreach ($comments as $comment) : ?>
                    <div class="bottom-comment"><!--bottom comment-->

                        <div class="comment-img">
                            <img class="img-circle" src="/public/images/comment-img.jpg" alt="">
                        </div>

                        <div class="comment-text">
                            <h5><?= $comment->name ?></h5>

                            <p class="comment-date">
                                <?= $comment->getDate(); ?>
                            </p>


                            <p class="para">
                                <?= $comment->text ?>
                            </p>
                        </div>
                    </div>
                    <!-- end bottom comment-->
                <?php endforeach; ?>

                <div class="leave-comment"><!--leave comment-->
                    <h4>Leave a reply</h4>

                    <?php $form = ActiveForm::begin([
                        'action' => ['post/view', 'id' => $article->id],
                        'options' => ['class' => 'form-horizontal contact-form', 'role' => 'form']]) ?>

                    <?= $form->field($comment, 'article_id')->hiddenInput(['value' => $article->id])->label(false) ?>

                    <div class="form-group">
                        <div class="col-md-6">
                            <!--                                Name-->
                            <?= $form->field($comment, 'name')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>

                    <div class="form-group">
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <!--                                Text-->
                            <?= $form->field($comment, 'text')->richTextArea(['options' => ['rows' => 6]]) ?>

                        </div>
                    </div>
                    <?= \yii\bootstrap\Html::submitButton('Send', ['class' => 'btn send-btn']) ?>
                    <?php ActiveForm::end(); ?>
                </div><!--end leave comment-->

            </div>
            <?= $this->render('/layouts/sidebar', [
                'recent' => $recent,
                'categories' => $categories,
                'popular' => $popular
            ]); ?>
        </div>
    </div>
</div>
<!-- end main content-->
<!--footer start-->